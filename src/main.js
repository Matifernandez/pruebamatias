import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'


import App from './App.vue'
import MatiasFernandez from '@/MatiasFernandez.vue'

const Home = {template: '<h1>Soy el Home</h1>'}

const routes = [
    {
        path: '/' , component: Home
    },
    {
        path: '/MatiasFernandez', component: MatiasFernandez
    }
]


const router = createRouter ({
    history: createWebHashHistory(),
    routes
})


const app = createApp(App)

app.use (router)

.mount('#app')
